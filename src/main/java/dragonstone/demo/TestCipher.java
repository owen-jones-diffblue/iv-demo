package dragonstone.demo;

import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Random;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class TestCipher {
    public byte[] Example1(Key key, byte[] input) throws Exception {
        Cipher cipher = Cipher.getInstance("AES/CTR/NoPadding");
        byte[] iv = new byte[16];
        new SecureRandom().nextBytes(iv);
        cipher.init(Cipher.ENCRYPT_MODE, key, new IvParameterSpec(iv));
        // Good! IV is securely randomly generated via SecureRandom()
        return cipher.doFinal(input);
    }

    public byte[] Example2(Key key, byte[] input) throws Exception {
        Cipher cipher = Cipher.getInstance("AES/CTR/NoPadding");
        cipher.init(Cipher.ENCRYPT_MODE, key);
        // Good! If the IV is not specified, it is securely randomly generated
        return cipher.doFinal(input);
    }

    public byte[] Example3(Key key, byte[] input) throws Exception {
        Cipher cipher = Cipher.getInstance("AES/CTR/NoPadding");
        byte[] iv = new byte[16];
        cipher.init(Cipher.ENCRYPT_MODE, key, new IvParameterSpec(iv));
        // Bad! IV is not securely randomly generated
        return cipher.doFinal(input);
    }

    public byte[] Example4(Key key, byte[] input) throws Exception {
        Cipher cipher = Cipher.getInstance("AES/CTR/NoPadding");
        byte[] iv = new byte[16];
        new Random().nextBytes(iv);
        cipher.init(Cipher.ENCRYPT_MODE, key, new IvParameterSpec(iv));
        // Bad! Random() is not secure, should have used SecureRandom()
        return cipher.doFinal(input);
    }

    public byte[] Example5(Key key, byte[] input) throws Exception {
        Cipher cipher = Cipher.getInstance("AES/CTR/NoPadding");
        byte[] iv = new byte[16];
        cipher.init(Cipher.DECRYPT_MODE, key, new IvParameterSpec(iv));
        // Good! IV does not need to be random for decryption
        return cipher.doFinal(input);
    }

    public byte[] Example6(Key key, byte[] input) throws Exception {
        Cipher cipher1 = Cipher.getInstance("AES/CTR/NoPadding");
        cipher1.init(Cipher.ENCRYPT_MODE, key);
        byte[] iv = cipher1.getIV();
        Cipher cipher2=Cipher.getInstance("AES/CTR/NoPadding");
        cipher2.init(Cipher.ENCRYPT_MODE, key, new IvParameterSpec(iv));
        // Good (but strange)! The IV is securely randomly generated
        // In the future, we may want to check for IV re-use
        return cipher2.doFinal(input);
    }

    public byte[] Example7(Key key, byte[] input) throws Exception {
        Cipher cipher1 = Cipher.getInstance("AES/CTR/NoPadding");
        cipher1.init(Cipher.ENCRYPT_MODE, key, new IvParameterSpec(new byte[16]));
        byte[] iv = cipher1.getIV();
        Cipher cipher2 = Cipher.getInstance("AES/CTR/NoPadding");
        cipher2.init(Cipher.ENCRYPT_MODE, key,new IvParameterSpec(iv));
        // Bad (and strange)! The IV is not securely randomly generated
        return cipher2.doFinal(input);
    }

    public byte[] Example8(SecretKeySpec key, byte[] input) throws Exception {
        byte[] iv = new byte[16];
        Cipher cipher = getCipher(iv, key);
        // Bad! Diffblue Secure can analyse whole program, not just function local
        return cipher.doFinal(input);
    }

    public byte[] LongExample(Key key, byte[] input) throws Exception {
        Cipher cipher = Cipher.getInstance("AES/CTR/NoPadding");
        byte[] iv = new byte[16];
        for(int i = 0; i < 100000000; ++i) { }
        cipher.init(Cipher.ENCRYPT_MODE, key, new IvParameterSpec(iv));
	return cipher.doFinal(input);
    }


    public static Cipher getCipher(byte[] iv, SecretKeySpec key) throws Exception {
        Cipher cipher = Cipher.getInstance("AES/CTR/NoPadding");
        cipher.init(Cipher.ENCRYPT_MODE, key, new IvParameterSpec(iv));
        // Too soon to tell, will depend on actual usage of this function
        return cipher;
    }
}
